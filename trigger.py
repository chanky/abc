import urllib3
from urllib import parse
import json
import os

# Define your GitLab URL and authentication token
GITLAB_ENDPOINT = os.getenv("CI_SERVER_URL")
PRIVATE_TOKEN = os.getenv("GITLAB_TOKEN")
urllib3.disable_warnings()

gl = gitlab.Gitlab(GITLAB_ENDPOINT, private_token=PRIVATE_TOKEN, ssl_verify=False)
# Define the group path
GROUP_PATH = os.getenv("FULL_GROUP_PATH")
# Fetch projects within the group (including subgroups)
group = gl.groups.get(GROUP_PATH, get_all=True, lazy=True)
group_projects = group.projects.list(include_subgroups=True, all=True, lazy=True, iterator=True)

for group_project in group_projects:
    try:
        project = gl.projects.get(group_project.id)
        # Create a trigger token  
        trigger = project.triggers.create({"description": "my description"})
        # Trigger a pipeline
        pipeline = project.trigger_pipeline(project.default_branch, token=trigger.token)
        print(f'Triggered pipeline for project "{project.name}" with "{project.default_branch}" branch.')
        # Delete the trigger token
        trigger.delete()
        print(f"Deleted trigger token for project: {project.name}")
    except Exception as e:
        print(f"Error triggering pipeline for project {project.name}: {str(e)}")


